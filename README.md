# Usage
###to create a wallet run
```
nodejs new-wallet.js
``` 

###to upload a file
```aidl
nodejs upload
```

###to mint a token
```aidl
nodejs mint
```

###to transfer to a user
```aidl
nodejs transfer
```

