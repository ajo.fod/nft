const FormData = require('form-data');
const axios = require('axios');
const fs = require('fs');

// https://masteringjs.io/tutorials/axios/form-data
const formData = new FormData();
formData.append('file', fs.createReadStream('/Users/ajo/Downloads/FFWD_Logo.png'));
const run = async () => {
    const res = await axios.post('http://click2nft.herokuapp.com/api/v1/artifact/upload', formData, {
        // You need to use `getHeaders()` in Node.js because Axios doesn't
        // automatically set the multipart form boundary in Node.
        headers: formData.getHeaders()
    });
    console.dir(res.data); // provides an artifact ID unique to an image.
    process.exit(0);
}

run();


