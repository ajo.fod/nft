const artifactId = 'c116dd1e-d79a-445e-8eea-ba8923a0b8db';
const axios = require('axios');

const run = async () => {
    const res = await axios.post(
        'http://click2nft.herokuapp.com/api/v1/nft/mint',
        {"artifactId":artifactId},
        );
    console.dir(res.data); // transaction ID
    process.exit(0);
}

run();


