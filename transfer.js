const artifactId = '73c4b619-f46a-4625-a1ef-fc5fd6fbc373';
const mintId = '0xf6b7622d48a1a3e768b6e4b2fe99c2fac7930a448bf3e81c23658301989e6940';

//Wallet code from ethereum-js
const toUserId = '0x3f8b7a4777fcb1924edfadb5848b6999c8c40f48';
const privateKey = '0x007e1a86a43c7198a60e067ec714cf5f175760144a60cf6b2736acb0d2222adc';

const web3 = require('web3');
const toWeb3UserId = web3.utils.toChecksumAddress(toUserId);

const axios = require('axios');

const run = async () => {
    const res = await axios.post(
        'https://click2nft.herokuapp.com/api/v1/nft/transfer',
        {
            "artifactId":artifactId,
            "mintId":mintId,
            "toUserId":toWeb3UserId,
            "blockchainData":{"blockchainNetwork":"rinkeby"},
        },
    );
    console.dir(res.data);
    process.exit(0);
}

run();